from unittest import TestCase
from io import StringIO
import sable_parser


class TestLexer(TestCase):
    def test_non_symbol1(self):
        f = StringIO("}")
        _lexer = sable_parser.Lexer(f)

        with self.assertRaises(sable_parser.LexerException):
            _lexer.next()
        f.close()

    def test_simple_symbols(self):
        f = StringIO("0 1 2 3 4 5 6 7 8 9 + - * / %")
        _lexer = sable_parser.Lexer(f)

        token = _lexer.next()
        self.assertEqual(token.getText(), '0', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '1', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '2', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '3', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '4', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '5', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '6', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '7', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '8', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '9', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '+', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '-', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '*', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '/', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '%', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertIsInstance(token, sable_parser.EOF)

        f.close()

    def test_compound_numbers_with_symbols(self):
        f = StringIO("123456789 + - * / %")
        _lexer = sable_parser.Lexer(f)

        token = _lexer.next()
        self.assertEqual(token.getText(), '123456789', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '+', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '-', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '*', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '/', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), ' ', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '%', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertIsInstance(token, sable_parser.EOF)

        f.close()

    def test_no_blanks(self):
        f = StringIO("12+34-56*78/9%6")
        _lexer = sable_parser.Lexer(f)

        token = _lexer.next()
        self.assertEqual(token.getText(), '12', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '+', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '34', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '-', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '56', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '*', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '78', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '/', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '9', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '%', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertEqual(token.getText(), '6', "Read token '" + token.__class__.__name__ + "'" + ", text = [" +
                         token.getText() + "] at [" + str(token.getLine()) + "," + str(token.getPos()) + "]")
        token = _lexer.next()
        self.assertIsInstance(token, sable_parser.EOF)

        f.close()
