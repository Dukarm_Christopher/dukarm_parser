# README #

### What is this repository for? ###

This repository contains the basic skeleton for CS426 - Introduction to Languages and Machines.  You
will clone this repository to your computer, upload it to your own bitbucket account, and share it with
your instructor.  All programming assignments for the course will be submitted using git push.

### How do I get set up? ###
To setup and run this repository, edit the `run_sable.py` script in the `utility_functions` folder.  

You will need to specify the fully qualified directory path for your repository in the variable `install_dir`, 
the relative directory of the compiler project you want to build in the varible `directory`, and the name of 
the SableCC lexer/parser file in the variable `filename`

Additionally, `run_sable.py` will run tests against your language specification.  In order to run tests, 
you will need specify the name of the test module in which you've put all of your unit tests in the 
variable `testModule`.  You will also need to specify the name of the test class you want to run from
within that module using the variable `testClass`.

Once you've made the appropriate modifications to these variables, you will be able to run `run_sable.py`
to generate all of your compiler files and then run unit tests against those files.

### Contribution guidelines ###
This repository is solely for cloning purposes.  Pushes won't be allowed by the permissions.

### Who do I talk to? ###

James Lotspeich
e-mail:  james.lotspeich@usafa.edu