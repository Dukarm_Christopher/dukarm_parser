import subprocess
import os
import importlib
import unittest
from io import StringIO
from pprint import pprint
import sys

############################ Parameters required for SableCC ########################### #
sablecc_dir = 'SableCC/sablecc-3-beta.3.altgen.20041114/'

# Modify this variable to match the directory of your repository
install_dir = 'C:\\Users\\C18Christopher.Dukar\\Documents\\Dukarm_LEXER'

# These two variables indicate the directory of the project you want to generate a compiler for and the name of the
# sableCC grammar file you wish to use
directory = 'PEX1'
filename = 'pex1.grammar'

# testModule is the name of the python file that you put your unit tests into
# testClass is the name of the actual testing class inside testModule
testModule = 'test_lexer_postfix'
testClass = 'TestLexer'

# Don't change anything below this line! #


# ############################ Run SableCC ########################### #
print('======================= Attempting to run SableCC =======================')
sable_only = False;

# Check for sane values for all strings above
if not os.path.isdir(install_dir):
    print('Error:  the directory ' + install_dir + ' does not exist.  ')
    print('Did you modify the value for \'install_dir\' in your run_sable.py script?')
    exit(-1)

if not os.path.isdir(install_dir + '\\' + directory):
    print('Error:  the directory ' + directory + ' does not exist in ' + install_dir + '.')
    print('Check the value of \'directory\' in your run_sable.py script')
    exit(-1)

if not os.path.exists(install_dir + '\\' + directory + '\\' + filename):
    print('Error:  grammar file ' + filename + ' does not exist.')
    print('Check the value of \'filename\' in your run_sable.py script')
    exit(-1)

if not os.path.exists(install_dir + '\\' + directory + '\\' + testModule + '.py'):
    print('Error:  test file ' + testModule + ' does not exist in ' + directory)
    print('Check the value for \'testModule\' in your run_sable.py script')
    print('Running SableCC without running tests')
    sable_only = True;


args = ['java', '-jar', sablecc_dir +'lib\\sablecc.jar', '-d', directory, '-t', 'python,python-build', directory +
        '\\' + filename]
p = subprocess.Popen(args, cwd=install_dir)
try:
    p.wait(5)
except subprocess.TimeoutExpired:
    print('Error running sablecc, the process timed out')
    exit(-1)

output, errors = p.communicate()

if p.returncode:
    print("Error running sablecc")
    print(errors.decode('utf-8'))
    exit(-1)
else:
    print('\r\n======================= SableCC completed with no errors =======================\r\n')

# ############################ Run Unittests and Output Results ########################### #
if not sable_only:
    sys.path.append(install_dir + '\\' + directory)
    testMod = importlib.import_module(testModule)
    print('======================= Running Unit Tests =======================')
    try:
        tester = getattr(testMod, testClass)
    except AttributeError:
        print('The file ' + testModule + '.py does not contain a class named \'' + testClass + '\'')
        print('Did you modify the value of \'testClass\' in your run_sable.py script?')
        print('\r\nAbandoning test run')
        exit(-1)

    stream = StringIO()
    runner = unittest.TextTestRunner(stream=stream)
    result = runner.run(unittest.makeSuite(tester))
    print("Tests run: ", result.testsRun)
    print("Errors: ", result.errors)
    pprint(result.failures)
    stream.seek(0)
    print("Test output\n", stream.read())